from lib.util import PyToPUIUtility
from Tkinter import *
from twisted.internet import tksupport, reactor

"""
TODO: send friendly name on tcp initiate
todo: encoding standards
"""


class PyToPApp:
    def add_peer(self, host= None, udp= 4444, tcp= 4444, faddr= None):
        if host == None and faddr== None:
            return None
        if faddr!=None:
            host, udp, tcp = PyToPUIUtility.faddr_to_address(faddr)
        if faddr == None and host != None and (tcp != None or udp != None):
            if udp == None:
                faddr= PyToPUIUtility.address_to_faddr(host, tcp, tcp)
            elif tcp == None:
                faddr= PyToPUIUtility.address_to_faddr(host, udp, udp)
            else :
                faddr= PyToPUIUtility.address_to_faddr(host, udp, tcp)

        for peer in self.peers:  # dont add if exists
            if peer["host"]== host:
                return
            if peer["faddr"] == faddr:
                return
        print "faddr add peer: ", faddr
        self.peers.append({'host': host,'tcp': tcp, 'udp': udp, 'faddr':faddr, 'fname':'n/a' })

    def setup_logging(self):
        #TODO  create log file
        import logging, os
        self.log = logging.getLogger("PyToP")
        logging.basicConfig(filemode = 'w')
        self.log.setLevel(logging.INFO)

        if not os.path.exists('tmp'):
            os.makedirs('tmp')


        # create a file handler
        handler = logging.FileHandler('tmp/app.log')
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        # add the handlers to the logger
        self.log.addHandler(handler)
        #self.log.info("i logged")

    def __init__(self, debug = False):

        from lib.app import PyToP
        from lib.ui.app import Out, MainWindow
        import Tkinter as tk
        from twisted.internet import tksupport
        self.log = None
        #todo determine my ip
        #todo determine ip history
        self.peers = [{'host': '127.0.0.1','tcp': 4444, 'udp': 4444, 'faddr':'1XIZI8CNPRWBG', 'fname':'me' }]
        #todo read settings
        self.settings = {
            'friendly_name': 'mursalat',
            'port':{ 'tcp': 4444, 'udp': 4444},
            'blocked_hosts': ['234.23.3.4'] # todo honor this
        }

        # logging:
        self.setup_logging()

        #tkinter/twisted event loop:

        self.tk_root = tk.Tk()
        tksupport.install(self.tk_root)
        # twisted:

        config = {
            'testing': {
                'tcp_port': 4444
            },
            'production': {
                'port': 2939
            }
        }
        outp = Out()

        self.net_app = PyToP(conf = config, out=outp, log = self.log, main_app = self)

        self.pytop_ui = MainWindow (self.tk_root, main_app = self)
        self.net_app.set_ui(self.pytop_ui)

        self.net_app.start_tcp_server()
        #self.net_app.start_tcp_client(peer_index = 0, text_message = "fuck yeah")

        # run:
        self.net_app.run_reactor()

if __name__ == "__main__":
    a = PyToPApp()
