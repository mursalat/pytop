from lib.app import PyToP
from lib.ui.app import Out

config = {
	'testing': {
		'tcp_port': 4444
    },
    'production': {
        'port': 2939
    }
}

outp = Out()
app = PyToP(config, outp)
app.start_tcp_server()
app.add_peer({'host':'localhost', 'tcp_port': 4444, 'udp_port':43582, 'friendly_name':'me-myself'})
app.start_tcp_client(0)
app.run_reactor()

