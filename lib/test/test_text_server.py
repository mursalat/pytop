from lib.p2p.text_server import TextFactory
from lib.ui.app import Out
from lib.app import PyToP
from twisted.trial import unittest
from twisted.test import proto_helpers

class TextServerTestCase(unittest.TestCase):
    def setUp(self):
        self.out = Out(display=False)
        self.app = PyToP({'testing': {'port': 4444}} , self.out)
        factory = TextFactory(self.app)
        self.proto = factory.buildProtocol(('127.0.0.1', 0))
        self.stringTransport = proto_helpers.StringTransport()
        self.proto.makeConnection(self.stringTransport)

    def test_connection_success(self):
        self.assertEquals(self.stringTransport.value(), "100\r\n")

    def test_text_message(self):
        self.stringTransport.clear()
        self.proto.lineReceived('201 hello world')
        self.assertEquals(self.stringTransport.value(), "101\r\n")

    def test_file_transfer(self):
        self.stringTransport.clear()
        file_name = "my file.doc"
        file_size = 10240
        self.proto.lineReceived('202 %s,%d' % (file_name, file_size ))
        self.assertEquals(self.stringTransport.value()[0:3], "102")
        send_token = self.stringTransport.value()[4:].strip()
        self.assertIn(send_token, self.proto.send_tokens, "send token not in server")
        self.stringTransport.clear()

        self.proto.lineReceived('203 %s' % (send_token))
        self.assertNotIn(send_token, self.proto.send_tokens, "send token should be removed from server")
        self.assertEquals(self.stringTransport.value().strip(), "103")
        self.assertTrue(self.proto.raw_mode, "is not in raw mode")
        self.stringTransport.clear()

        #TODO test raw data sending

        self.proto.lineReceived('204')
        self.assertEquals(self.stringTransport.value().strip(), '104')
        self.assertFalse(self.proto.raw_mode, "is not in line mode")


#    def test_file_name(self):
