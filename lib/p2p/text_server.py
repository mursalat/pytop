from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

class TextProtocol(LineReceiver):
    #TODO handle lines that are too long
    def __init__ (self, app, addr):
        self.file_name = None
        self.file_size = None
        self.send_tokens = []
        self.raw_mode = False
        self.app = app
        self.addr = addr

    def _generateSendToken(self):
        from uuid import uuid4
        token = uuid4().hex
        self.send_tokens.append(token)
        return token

    def connectionMade(self):
        self.app.log.info("Client connected: "+self.addr.host )
        self.sendLine("100")
    
    def lineReceived(self, line):
        from lib.util import Util
        self.app.log.info("Server: New Line: "+ line)
        self.app.app.add_peer(host = self.addr.host)
        status = line[0:3]
        if status == "201": # text message
            self.app.ui.text_message_received(message = line[4:], peer= self.app.peers[Util.deduce_host_index(self.addr.host, self.app.peers)])
            self.sendLine("101")
        elif status == "202": # client sending file
            self.file_name = line[4:].split(',')[0]
            self.file_size = line[4:].split(',')[1]
            if self.app.out.tcp_confirm_new_file(self.file_name, self.file_size) :
                #user confirmed to accept file
                self.sendLine("102 %s" % (self._generateSendToken()) )
            else:
                self.sendLine("152")
        elif status == "203": # initiate raw mode
            if line[4:] in self.send_tokens:
                self.raw_mode= True
                self.setRawMode()
                self.send_tokens.remove(line[4:].strip())
                self.sendLine("103")
            else:
                self.sendLine("153")
        elif status == "204": # change to line mode
            if self.raw_mode:
                self.setLineMode()
                self.sendLine("104")
                self.raw_mode = False
            else:
                self.sendLine("154")
        elif status == "205": # peer started UDP stream to me
            pindex = Util.deduce_host_index(self.addr.host, self.app.peers)
            if pindex is not None:
               self.app.peers[pindex]['meta']['udp_content_type']= line[4:]

        
    def rawDataReceived(self, data): # raw mode is only for receiving files
        if self.file_name != None :
            # store the file
            self.app.out.store_file(self.file_name, data)
            self.setLineMode()
            # remove file name and size info
            self.file_name = None
            self.file_size = None
    
    def connectionLost(self, reason):
        self.app.log.info("Client disconnected: "+self.addr.host )

class TextFactory(Factory):
    def __init__(self, app):
        self.app = app
        
    def buildProtocol(self, addr):
        return TextProtocol(self.app, addr)
