from twisted.internet.protocol import Factory, Protocol
from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver
from lib.util import Util

class TextProtocol(LineReceiver):
    def __init__(self, out, app, peer_index, text_message = None, send_file_location = None, initiate_udp = None ):
        self.out = out
        self.app = app
        self.s_token = None
        self.peer_index = peer_index
        self.text_message = text_message
        self.send_file_location = send_file_location
        self.initiate_udp = initiate_udp
    
    def connectionMade(self):
        self.sendLine('200')
        if self.text_message != None :
            self.sendMessage()
        if self.send_file_location != None :
            self.send_file()
        if self.initiate_udp != None:
            self.started_udp()


    def sendMessage(self):
        self.app.log.info("Sending message to : "+ str(self.app.peers[self.peer_index]))
        self.sendLine("201 " + self.text_message)
        self.text_message = None
        #self.transport.loseConnection()

    def initiateRawDataMode(self):
        if self.s_token == None :
            self.out.error("TCP CLIENT: file sending token not set")
        else:
            self.sendLine("203 %s" % (self.s_token))

    def started_udp(self):
        self.sendLine("205 "+str(self.initiate_udp))

    def send_file_data(self):
        try:
            opf= open(self.send_file_location)
        except IOError:
             self.out.error("couldn't open file "+ self.send_file_location)
        # send the file content
        else:
                self.transport.write(opf.read())
                self.transport.loseConnection()

    def send_file(self):
        import os
        if self.send_file_location == None :
            self.out.error("send file location is none")

        else :
            f_name = self.send_file_location.split("/")[-1]
            self.sendLine("202 "+Util.safe_file_name(f_name)+","+str(os.stat(self.send_file_location).st_size))
    
    def dataReceived(self, data):
        if data[0:3] == "102":
            self.s_token = data[4:].strip()
            self.initiateRawDataMode()
            self.send_file_data()

        else:
            self.out.client_received(data)

        
class TextFactory(Factory):
    def __init__(self, out, app, peer_index, **kwargs ):
        self.out = out
        self.dictKargs = kwargs
        self.app = app
        self.peer_index = peer_index

    def buildProtocol(self, addr):
        p = TextProtocol(self.out, self.app, self.peer_index, **(self.dictKargs))
        p.factory = self
        return p

    def startedConnecting(self, connector):
        self.app.log.info("Client started connection")

    def clientConnectionLost(self, connector, reason):
        self.app.log.info("Client lost connection")

    def clientConnectionFailed(self, connector, reason):
        self.app.log.info("Client failed to connect")

