from twisted.internet.protocol import DatagramProtocol

class StreamHandling(DatagramProtocol):
    def __init__(self, out, app, peer_index, **kwargs):
        self.app = app
        self.peer_index = peer_index
        self.out = out
        self.producer = kwargs['producer'](self)

    def startProtocol(self):
        self.transport.connect(self.app.peers[self.peer_index]["ip"], self.app.peers[self.peer_index]["udp"])
        self.transport.registerProducer(self.producer, True)
        self.producer.resumeProducing()

    def datagramReceived(self, data, (host, port)):
        print "UDP GOT: ", str(data)
