class Util:
    @staticmethod
    def deduce_peer_index((host, port), peers, type="tcp"):
        index = 0
        for peer in peers:
            #print "comparing ", peer[type+"_port"], " to ", port, " | host ", peer["host"], " to ", host
            if peer[type+"_port"] == port and peer["host"] == host :
                return index
            index += 1
        return None

    @staticmethod
    def deduce_host_index(ipv4host, peers):
        index = 0
        for peer in peers:
            if peer['host'] == ipv4host :
                return index
            index +=1
        return None

    @staticmethod
    def safe_file_name(name):
        return name.translate(None, ",")


class PyToPUIUtility:
    class SettingsHandler:
        """
        Read/Write Settings
        """
        def read_settings(self, profile_name):
            #TODO read xml them into dictionary
            return True

        def write_settings(self, profile_name):
            #TODO write settings to xml
            return True

        def __init__(self, settings_floc = "settings.xml"):
            self.settings_file = settings_floc
            self.settings = {}

    class KeyValueReader:
        """
        To read key value pairs from xml file
        """
        def get_value(self, key):
            return True

        def __init__(self, floc):
            self.file_location = floc
            #TODO read into memory

    #from : http://en.wikipedia.org/wiki/Base_36#Python_implementation [21.12.2014]
    @staticmethod
    def base36encode(number, alphabet='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
        """Converts an integer to a base36 string."""
        if not isinstance(number, (int, long)):
            raise TypeError('number must be an integer')

        base36 = ''
        sign = ''

        if number < 0:
            sign = '-'
            number = -number

        if 0 <= number < len(alphabet):
            return sign + alphabet[number]

        while number != 0:
            number, i = divmod(number, len(alphabet))
            base36 = alphabet[i] + base36

        return sign + base36

    @staticmethod
    def base36decode(number):
        return int(number, 36)

    @staticmethod
    def address_to_faddr(ip, udp_port, tcp_port):
        """
        friendly address generator
        :param ip: ip address
        :param udp_port: udp port number
        :param tcp_port: tcp port number
        :rtype: base 36 string adddress
        """
        ip_bin = ''.join([bin(int(x)+256)[3:] for x in ip.split('.')])
        udp_port_bin = bin(int(udp_port)+65536)[3:]
        tcp_port_bin = bin(int(tcp_port)+65536)[3:]
        return PyToPUIUtility.base36encode(int(ip_bin+udp_port_bin+tcp_port_bin,2))

    @staticmethod
    def faddr_to_address(faddr):
        """
        friendly address parser
        :param faddr: friendly address
        :rtype: tuple (ip, udp port, tcp port)
        """
        faddr_bin = bin(PyToPUIUtility.base36decode(faddr))[2:]
        return (
            #ip
            '.'.join((
                str(int(faddr_bin[:-56][-8:],2)),
                str(int(faddr_bin[:-48][-8:],2)),
                str(int(faddr_bin[:-40][-8:],2)),
                str(int(faddr_bin[:-32][-8:],2))
            )),
            #udp port
            str(int(faddr_bin[-32:][:16], 2)),
            #tcp port
            str(int(faddr_bin[-16:], 2))
        )
    @staticmethod
    def peer_index(peers, host= None, faddr= None, tcp= None, udp= None):
        """
        get peer index on ui
        :param peers:
        :param host:
        :param tcp:
        :param udp:
        :return: peer element from peers
        """
        if faddr != None and host == None:
            host, udp, tcp = PyToPUIUtility.faddr_to_address(faddr)
        #todo refactor and use this more
        indx = 0
        for peer in peers:
            if peer["host"]== host:
                return indx
            if peer["faddr"]== faddr:
                return indx
            indx = indx + 1

        return None
    @staticmethod
    def peer_index_portcomb(peers, (host, port)):
        return PyToPUIUtility.peer_index(peers, tcp = port, host = host)
