from twisted.internet import reactor, error

class PyToP():
    def __init__(self, conf, out, ui = None, log = None, main_app = None):
        self.out = out
        self.conf = conf
        self.app = main_app
        self.ui = ui
        self.log = log
        #self.peers = []
        self.peers = main_app.peers
        self.environment = 'testing'

    def set_ui(self, ui):
        self.ui = ui

    def start_tcp_server(self):
        from lib.p2p.text_server import TextFactory
        try:
            reactor.listenTCP(port = self.conf[self.environment]['tcp_port'], factory = TextFactory(app = self))
        except error.CannotListenError as e:
            self.log.error(e)
            print e

    def add_peer(self, peer):
        self.peers.append(peer)

    def start_tcp_client(self, peer_index, **kwargs):
        from lib.p2p.text_client import TextFactory
        reactor.connectTCP(self.peers[peer_index]['host'], int(self.peers[peer_index]['tcp']), TextFactory(out = self.out, app = self, peer_index=peer_index, **kwargs) )

    def run_reactor(self):
        reactor.run()

    def stop_reactor(self):
        reactor.stop()
