__author__ = 'mursalat'
# todo: video stuff + mac support + maybe linux support
from sys import platform as _PLATFORM_
not_supported= lambda : "Your platform (",_PLATFORM_,") is not supported"

class FFMPEGWrapper():
    def __init__(self, app = None, populate_devices= False):
        self.app = app
        self.ffmpeg_bin = 'C:/Users/mursalat/Documents/sc/pytop-19-7-2014/pytop/bin/ffmpeg-20141228-git-202947a-win32-static/bin'
        #self.ffmpeg_bin = 'bin/ffmpeg-20141212-git-10ef8f0-win32-static/bin'
        self.ffmpegl= self.ffmpeg_bin+"/ffmpeg"
        self.ffplayl= self.ffmpeg_bin+"/ffplay"

        self.running_process = []
        self.devices = {"video": [], "audio":[]}
        self.default_audio_device_index = 0
        self.default_video_device_index = 0

        if populate_devices:
            self.get_list_of_devices()

        self.capture_audio = False

        #settings:

    def stop_capture_audio(self):
        self.capture_audio = False

    def dosomething(self):
        with self.run_command([self.ffmpegl, 'pipe']).stdout as ffmpeg:
            print ffmpeg.read()

    def start_play_audio_proc(self):
        import time
        command = None
        if _PLATFORM_== "win32":
            command = [self.ffplayl,'-i','-nodisp', 'pipe:0']
        else:
            print not_supported()
        try:
            proc = self.run_command(command, bufsize=0)
        except WindowsError as we:
            print we

        #todo take audio stream to play
        with open("C:/Users/mursalat/Documents/sc/pytop-19-7-2014/pytop/bin/ffmpeg-20141212-git-10ef8f0-win32-static/bin/myv.mp3", 'r+b') as tr:
            for d in tr:
                proc.stdin.write(d)
        #todo maybe fix issue with data being received faster than audio played
        time.sleep(0.5) # wait for audio playing to finish
        proc.kill()

    def start_capture_audio_proc(self, callback= None):
        self.capture_audio= True
        command = None
        if _PLATFORM_ == 'win32':
            #todo supprot mac
            command = [self.ffmpegl,
                    '-f', 'dshow',
                    '-i','audio='+self.devices["audio"][self.default_audio_device_index],
                    '-f', 's16le',
                    '-acodec', 'libmp3lame',
                    '-']
        else:
            print not_supported()
        proc = self.run_command(command, bufsize=0)
        with proc.stdout as op, proc.stdin as ip, proc.stderr as ep:
            while self.capture_audio:
                #to_write.write(op.read(32))
                callback(op.read(32))
            ip.write('q\n')
        proc.kill()

    def get_list_of_devices(self):
        command = None
        if _PLATFORM_ == 'win32':
            command = [self.ffmpegl,
                    '-list_devices', 'true',
                    '-f','dshow',
                    '-i', 'dummy',
                    '-']
        proc = self.run_command(command, bufsize=0)
        with proc.stdout as op, proc.stdin as ip, proc.stderr as ep:
            vid_flag = False
            ds_info = None
            for line in ep:
                if _PLATFORM_ == 'win32':
                    if line[:6] == '[dshow':
                        ds_info = line[18:].strip()
                        if ds_info.split(' ')[1] == "video":
                            vid_flag= True
                        elif ds_info.split(' ')[1] == "audio":
                            vid_flag = False
                        else:
                            if vid_flag:
                                self.devices["video"].append(ds_info.replace('"', ''))
                            else:
                                self.devices["audio"].append(ds_info.replace('"', ''))
                #todo support mac
                else:
                    print not_supported()

    def run_command(self, arguments, bufsize = 0):
        """
        run subprocess
        :param arguments: arguments to pass
        :return: runnning process
        """
        import subprocess as sp
        proc = sp.Popen(arguments, stdout = sp.PIPE, stdin=sp.PIPE, stderr=sp.PIPE, bufsize=bufsize)
        self.running_process.append(proc)
        return proc

    def __del__(self):
        if FFMPEGWrapper:
            if len(self.running_process) > 0 :
                for proc in self.running_process:
                    proc.kill()

"""
from threading import Timer


of = open("myv2.mp3", "a+b", buffering = -1)
def mcback (datastream):
    of.write(datastream)
def closefile():
    of.close()


a = FFMPEGWrapper(populate_devices=True)

t1 = Timer(0.1, a.start_capture_audio_proc, [mcback])
t2 = Timer(10.0, a.stop_capture_audio)
t3 = Timer(11.0, closefile)
t1.start()
t2.start()
t3.start()

a.start_play_audio_proc()
"""