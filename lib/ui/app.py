#TODO: make gui
from zope.interface import implements
from twisted.internet import interfaces
from random import randrange

class Out():
    def __init__(self, display = True):
        self.display = display

    def new_message(self, msg):
        if self.display:
            print "SERVER MESSAGE: ", msg

    def tcp_confirm_new_file(self, fname, fsize):
        if self.display:
            confirmation = raw_input("FILE: accept file (%s, %d) ? (yes/no) " % (fname, int(fsize)) )
            if confirmation.lower() == "yes":
                return True
        #TODO for testing i am using true by default
        return True

    def store_file(self, fname, data):
        store_in="/home/mursalat/Downloads/"
        if self.display:
            print "STORING FILE: ", fname
        f = open(store_in+fname, 'a')
        f.write(data)
        f.close()

    def error(self, errmsg):
        if self.display:
            print "ERROR: ", errmsg

    def server_client_connected(self):
        if self.display:
            print "SERVER: DEBUG: connection made"

    def server_client_disconnected(self):
        if self.display:
            print "DEBUG: connection lost"

    def client_received(self, data):
	    print "CLIENT: RECEIVED: ", data

#
# Copied off of twisted's streaming example, with minimal changes for proof of concept
#
class ExamplePushProducer(object):
    """
    Send back the requested number of random integers to the client.
    """

    implements(interfaces.IPushProducer)

    def __init__(self, proto):
        self._proto = proto
        self._goal = 1000
        self._produced = 0
        self._paused = False

    def pauseProducing(self):
        """
        When we've produced data too fast, pauseProducing() will be called
        (reentrantly from within resumeProducing's sendLine() method, most
        likely), so set a flag that causes production to pause temporarily.
        """
        self._paused = True
        print 'Pausing connection from %s' % self._proto.transport.getPeer()

    def resumeProducing(self):
        """
        Resume producing integers.

        This tells the push producer to (re-)add itself to the main loop and
        produce integers for its consumer until the requested number of integers
        were returned to the client.
        """
        self._paused = False
        self._proto.transport.write("talk to u later");

        while not self._paused and self._produced < self._goal:
            next_int = randrange(0, 10000)
            self._proto.transport.write('%d' % next_int)
            self._produced += 1

        if self._produced == self._goal:
            self._proto.transport.unregisterProducer()
            #self._proto.transport.loseConnection()

    def stopProducing(self):
        """
        When a consumer has died, stop producing data for good.
        """
        self._produced = self._goal

###
###
### TODO cleanup above
###
###
import Tkinter as tk
from lib.util import PyToPUIUtility


class CommunicationWindow(tk.Toplevel):
    def create_ui(self):
        import ScrolledText as st
        self.chat_st = st.ScrolledText(self, height=15, width = 30, wrap = tk.WORD)
        self.chat_st.config(state=tk.DISABLED)
        self.chat_st.grid(row = 0, column = 0, columnspan = 2)

        self.new_message_l = tk.Label(self, text="Send Message: ")
        self.new_message_l.grid(row = 1, column=0, sticky = tk.W)

        self.new_message_t = tk.Text(self, height=2, width = 24)
        self.new_message_t.grid(row = 2, column=0, sticky = tk.W)
        self.new_message_t.focus()
        self.new_message_t.bind('<Return>', self.send_message)

        self.new_message_b = tk.Button(self, text="Send")
        self.new_message_b.grid(row = 2, column=1,sticky = tk.E)


    def send_message(self, arg2): # TODO add text message
        print self.master.app.peers
        self.add_chat_message("me", self.new_message_t.get(1.0, tk.END))
        self.master.net_app.start_tcp_client(peer_index = PyToPUIUtility.peer_index(self.master.app.peers, faddr =self.peer_faddr ), text_message = self.new_message_t.get(1.0, tk.END).encode('utf'))
        return "break"

    def add_chat_message(self, from_fname, message):
        self.chat_st.config(state=tk.NORMAL)
        self.chat_st.insert(tk.INSERT, from_fname+": "+message.rstrip()+"\n")
        self.chat_st.config(state=tk.DISABLED)
        return True

    def __init__(self, master, peer_faddr):
        tk.Toplevel.__init__(self, master = master)
        self.peer_faddr = peer_faddr
        # todo remove peer when destroyed
        if PyToPUIUtility.peer_index(self.master.app.peers, faddr=peer_faddr) == None:
            self.master.app.add_peer(faddr= peer_faddr)
        self.wm_title(peer_faddr)
        self.wm_protocol("WM_DELETE_WINDOW", lambda: self.master.cleanup_communicationwindow(peer_faddr))
        self.create_ui()

class MainWindow(tk.Frame):
    def create_ui(self):
        self.my_address = []

        self.my_address_l = tk.Label (self.master, text = "Your Address: "+PyToPUIUtility.address_to_faddr('84.249.200.191',43582,4444))
        self.my_address_l.grid(row = 0, column= 0, columnspan = 2)

        self.peer_to_call_l = tk.Label(self.master, text="Peer:")
        self.peer_to_call_l.grid(row = 1 , column = 0)

        self.peer_to_call = tk.StringVar()
        self.peer_to_call_e = tk.Entry(self.master, textvariable = self.peer_to_call)
        self.peer_to_call_e.grid(row = 1 , column = 1, columnspan = 1)

        self.peer_to_call_list_choice = tk.StringVar(self.master)
        self.peer_to_call_list_choice.set(None)
        self.peer_to_call_list_o = tk.OptionMenu(self.master, self.peer_to_call_list_choice , PyToPUIUtility.address_to_faddr('127.0.0.1',43582,4444), command=self.set_peer)
        self.peer_to_call_list_o.grid(row = 2, column= 0, columnspan = 2)

        self.peer_contact_b = tk.Button(self.master, text="Contact", command = self.button_contact)
        self.peer_contact_b.grid(row = 3, column = 0, columnspan = 2)


    def set_peer(self, argv): # change peer to drop down selected one
        self.peer_to_call.set(self.peer_to_call_list_choice.get())

    def button_contact(self):
        #todo add peer to list of peers
        faddr = None
        #todo default address format
        #simplified address:
        if ":" in self.peer_to_call.get() or "." in self.peer_to_call.get():
            # simple format ip:tcp:udp
            parts = self.peer_to_call.get().split(':')

            #checp if ip match any peers

            if len(parts)== 1:
                faddr= None
            if len(parts) == 2 :
                faddr = PyToPUIUtility.address_to_faddr(parts[0], parts[1], parts[1])
            if len(parts) == 3:
                faddr = PyToPUIUtility.address_to_faddr(parts[0], parts[2], parts[1])

            for peer in self.peers:
                if peer["host"] == parts[0]:
                    faddr = peer['faddr']

        #todo: friendly address detection
        if len(self.peer_to_call.get()) > 5 and faddr == None :
            faddr = self.peer_to_call.get()
        self.open_communication_window(ptc_faddr=faddr)


    def text_message_received(self,  message, peer):
        #print "msg rec: ", message, str(peer)
        w = self.open_communication_window(ptc_host =peer['host'])
        w.add_chat_message(peer['fname'], message = message)

    def file_received(self, peer_faddr):
        #todo show file received
        return True

    def open_communication_window(self, peer_to_call = None, ptc_faddr = None, ptc_host = None  ):
        """
        :param peer_to_call: friendly address of peer
        :return: communication window
        """
        p_tcall_faddr= None
        if peer_to_call != None:
            p_tcall_faddr = peer_to_call # argument
        elif ptc_faddr != None:
            p_tcall_faddr= ptc_faddr
        elif ptc_host != None:
            #todo find host based window
            p_tcall_faddr = self.peers[PyToPUIUtility.peer_index(self.peers, host = ptc_host)]['faddr']
        else:
            self.log.warning("ui : couldn't set faddr")

        if p_tcall_faddr != None and "CW"+p_tcall_faddr not in self.windows.keys()  :
                self.windows["CW"+p_tcall_faddr] = CommunicationWindow(self, peer_faddr = p_tcall_faddr )

        elif p_tcall_faddr != None:
            self.windows["CW"+p_tcall_faddr].focus()
        else:
            self.log.info("window wasn't created nor selected")
            return None
        return self.windows["CW"+p_tcall_faddr]


    def cleanup_communicationwindow (self, peer_faddr):
        self.windows["CW"+peer_faddr].destroy()
        del self.windows["CW"+peer_faddr]

    def validate_peer_address(self):
        #todo validate input address
        return True

    def before_close(self):
        self.net_app.stop_reactor()

    def __init__(self, root, main_app):
        tk.Frame.__init__(self, master = root)
        self.app = main_app
        self.log = self.app.log
        self.peers = self.app.peers
        self.net_app = self.app.net_app
        self.master.protocol("WM_DELETE_WINDOW", self.before_close)

        self.windows = {}
        self.master.title("PyToPUI")
        self.create_ui()

#todo settings ui
class SettingsWindow(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self, master=parent)
        self.title("PyToP Settings")

#todo dialog ui
class Dialogs:
    class Information(tk.Toplevel):
        STATUS_ERROR = 1
        STATUS_INFO = 2
        STATUS_OTHER = 3
        def __init__(self, parent, message, status = 2 ):
            tk.Toplevel.__init__(self, master=parent)
            self.title("PyToP Settings")